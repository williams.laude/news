// Grab elements
const SelectElement = selector => {
    const element = document.querySelector(selector);
    if (element) return element;
    throw new Error(`Something went, make sure that ${selector} exists or is typed correctly.`);
};

//Nav styles on scroll
const scrollHeader = () =>{
    const headerElement = SelectElement('#header');
    if(this.scrollY >= 15){
        headerElement.classList.add('activated');
    }else {
        headerElement.classList.remove('activated');
    }
}

window.addEventListener('scroll', scrollHeader);
// Open menu & search pop-up
const MenuToggleIcon = SelectElement('#menu-toggle-icon');
const toogleMenu = () => {
    const mobileMenu = SelectElement('#menu');
    mobileMenu.classList.toggle('activated');
    MenuToggleIcon.classList.toggle('activated');
}

MenuToggleIcon.addEventListener('click',  toogleMenu);
// Open/Close search form popup
const formOpenBtn = SelectElement('#search-icon');
const formCloseBtn = SelectElement('#form-close-btn');
const searchFormContainer = SelectElement('#search-form-container')

formOpenBtn.addEventListener('click', () => searchFormContainer.classList.add('activated'));
formCloseBtn.addEventListener('click', () => searchFormContainer.classList.remove('activated'));
// -- Close the search form popup on ESC keypress
window.addEventListener('keyup', event => {
    if(event.key === 'Escape') searchFormContainer.classList.remove('activated');
})
// Switch theme/add to local storage
const bodyElement = document.body;
const themeToggleBtn = SelectElement('#theme-toggle-btn');
const currentTheme = localStorage.getItem('currentTheme');

if(currentTheme){
    bodyElement.classList.add('light-theme');
}

themeToggleBtn.addEventListener('click', () => {
    bodyElement.classList.toggle('light-theme');

    if(bodyElement.classList.contains('light-theme')){
        localStorage.setItem('currentTheme','themeActive');
    }else{
        localStorage.removeItem('currentTheme');
    }
})
// Swiper